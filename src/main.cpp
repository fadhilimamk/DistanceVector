#include <stdio.h>

int N;      // jumlah node pada topologi   
int E;      // jumlah edge
int S;      // jumlah skenario
int distance[1000][1000];
int hoop[1000][1000];

void initializeTable();
void printAllTables();
void sendInfo(int x, int y);

int main(int argc, char* argv[]) {
    int x,y;

    scanf("%d %d", &N, &E);
    initializeTable();
    
    // Baca semua edge
    while (E--) {
        scanf("%d %d", &x, &y);
        distance[x-1][y-1] = 1;
        hoop[x-1][y-1] = y-1;

        distance[y-1][x-1] = 1;
        hoop[y-1][x-1] = x-1;
    }
    
    // Masukan jumlah skenario
    scanf("%d", &S);
    while(S--) {
        scanf("%d %d", &x, &y);
        sendInfo(x-1,y-1);
    }
    printAllTables();

    return 0;
}

void sendInfo(int x, int y) {
    int dist = distance[y][x]; int hop = hoop[y][x];
    if (dist == -1 || hop == -1)
        return;
    for (int i = 0; i < N; i++) {
        if (distance[x][i] != -1 && (distance[x][i]+dist < distance[y][i] || distance[y][i] == -1)) {
                distance[y][i] = distance[x][i]+dist;
                hoop[y][i] = hop;
        } else if (distance[x][i]+dist == distance[y][i] && hop < hoop[y][i]) {
            hoop[y][i] = hop;
        }
    }
}

void printAllTables() {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            int next = hoop[i][j];
            if (next != -1) {
                next = next + 1;
            }
            printf("%d %d\n", distance[i][j], next);
        }
    }
}

void initializeTable() {
    for (int i = 0; i < N; i++) {
        for (int j = i; j < N; j++) {
            distance[i][j] = (i == j) ? 0 : -1;
            hoop[i][j] = (i == j) ? i : -1;
            
            distance[j][i] = (i == j) ? 0 : -1;
            hoop[j][i] = (i == j) ? i : -1;
        }
    }
}